package ru.pkonovalov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.IProjectDTORepository;
import ru.pkonovalov.tm.api.repository.dto.ITaskDTORepository;
import ru.pkonovalov.tm.api.service.dto.IProjectTaskDTOService;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.repository.dto.ProjectDTORepository;
import ru.pkonovalov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    private IConnectionService connectionService;

    public ProjectTaskDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(em);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            taskRepository.removeAllBinded(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = new TaskDTORepository(em);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(em);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(em);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            @Nullable final String projectId = projectRepository.getIdByIndex(userId, projectIndex);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, projectIndex);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(em);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            @Nullable final String projectId = projectRepository.getIdByName(userId, projectName);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeByName(userId, projectName);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(em);
            taskRepository.unbindTaskFromProject(userId, taskId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
