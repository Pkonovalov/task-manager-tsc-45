package ru.pkonovalov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.dto.ProjectDTO;
import ru.pkonovalov.tm.dto.SessionDTO;
import ru.pkonovalov.tm.dto.TaskDTO;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.model.Project;
import ru.pkonovalov.tm.model.Session;
import ru.pkonovalov.tm.model.Task;
import ru.pkonovalov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;
    @NotNull
    private final IPropertyService propertyService;


    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = factory();
    }

    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl());
        settings.put(Environment.USER, propertyService.getJdbcUsername());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());

        settings.put(Environment.DIALECT, propertyService.getJdbcDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getJdbcHBM2DDL());
        settings.put(Environment.SHOW_SQL, propertyService.getJdbcShowSql());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLevelCash());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProvider());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClass());
        settings.put(HIBERNATE_LITEMEMBER_KEY, propertyService.getLiteMember());


        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);

        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);

        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);

        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
