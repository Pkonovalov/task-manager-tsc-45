package ru.pkonovalov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.model.IAbstractService;
import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;
import ru.pkonovalov.tm.model.AbstractBusinessEntity;

public abstract class AbstractService<E extends AbstractBusinessEntity> implements IAbstractService<E> {

    @NotNull
    private IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
