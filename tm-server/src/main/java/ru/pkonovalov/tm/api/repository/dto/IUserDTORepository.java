package ru.pkonovalov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.UserDTO;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

}
