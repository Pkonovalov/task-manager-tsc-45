package ru.pkonovalov.tm.api.service.dto;

import ru.pkonovalov.tm.api.repository.dto.IAbstractDTORepository;
import ru.pkonovalov.tm.dto.AbstractEntityDTO;

public interface IAbstractDTOService<E extends AbstractEntityDTO> extends IAbstractDTORepository<E> {

}
