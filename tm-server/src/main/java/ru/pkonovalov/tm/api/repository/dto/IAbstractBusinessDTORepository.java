package ru.pkonovalov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;

import java.util.List;

public interface IAbstractBusinessDTORepository<E extends AbstractBusinessEntityDTO> extends IAbstractDTORepository<E> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<E> findAll(@Nullable String userId);


    @Nullable
    E findById(@Nullable String userId, @Nullable String id);

    @Nullable
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    String getIdByIndex(@Nullable String userId, @Nullable Integer index);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull Long size(@Nullable String userId);

}
