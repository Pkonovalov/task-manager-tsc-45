package ru.pkonovalov.tm.api.service.dto;

import ru.pkonovalov.tm.api.repository.dto.IAbstractBusinessDTORepository;
import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;

public interface IAbstractBusinessDTOService<E extends AbstractBusinessEntityDTO> extends IAbstractBusinessDTORepository<E> {

}
