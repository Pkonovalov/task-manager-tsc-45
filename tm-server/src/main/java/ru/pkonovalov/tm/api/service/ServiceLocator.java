package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.service.dto.*;

public interface ServiceLocator {

    @NotNull IAuthService getAuthService();

    @NotNull IConnectionService getConnectionService();

    @NotNull IProjectDTOService getProjectService();

    @NotNull IProjectTaskDTOService getProjectTaskService();

    @NotNull IPropertyService getPropertyService();

    @NotNull ISessionDTOService getSessionService();

    @NotNull ITaskDTOService getTaskService();

    @NotNull IUserDTOService getUserService();

}
