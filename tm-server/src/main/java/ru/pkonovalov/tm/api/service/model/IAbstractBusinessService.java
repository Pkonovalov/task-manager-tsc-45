package ru.pkonovalov.tm.api.service.model;

import ru.pkonovalov.tm.api.repository.model.IAbstractBusinessRepository;
import ru.pkonovalov.tm.model.AbstractBusinessEntity;

public interface IAbstractBusinessService<E extends AbstractBusinessEntity> extends IAbstractBusinessRepository<E> {

}
