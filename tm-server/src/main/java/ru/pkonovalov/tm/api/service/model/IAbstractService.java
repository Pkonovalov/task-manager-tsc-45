package ru.pkonovalov.tm.api.service.model;

import ru.pkonovalov.tm.api.repository.model.IAbstractRepository;
import ru.pkonovalov.tm.model.AbstractEntity;

public interface IAbstractService<E extends AbstractEntity> extends IAbstractRepository<E> {

}

