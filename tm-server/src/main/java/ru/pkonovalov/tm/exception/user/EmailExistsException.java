package ru.pkonovalov.tm.exception.user;

import ru.pkonovalov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

}
