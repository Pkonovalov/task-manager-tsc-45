package ru.pkonovalov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.entity.IHasDateStart;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByStarted implements Comparator<IHasDateStart> {

    private static @NotNull
    final ComparatorByStarted INSTANCE = new ComparatorByStarted();

    public static @NotNull ComparatorByStarted getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasDateStart o1, @Nullable final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStart() == null) {
            if (o2.getDateStart() == null) return 0;
            else return 1;
        }
        if (o2.getDateStart() == null) {
            if (o1.getDateStart() != null) return -1;
            else return 1;
        }
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
