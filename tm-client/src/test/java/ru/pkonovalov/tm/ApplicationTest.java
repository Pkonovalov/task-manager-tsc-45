package ru.pkonovalov.tm;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.marker.UnitCategory;

public class ApplicationTest {

    @Test
    @Category(UnitCategory.class)
    public void test() {
        Assert.assertNotNull(Application.class);
    }

}
